/**
 * Gestionaire 
 * cette classe permet d acceder aux données des voitures, de reservations et 
 * membre
 * ATTENTION: pour fonctionner elle est omblige d implemante les données a gerer
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo3.backend;

import java.util.ArrayList;

public class Gestionaire implements Data {

    
    public Gestionaire(){
        try{
                if(CLIENTS == null) throw new DataException("Impossible de charger la liste de clients");
                if(VOITURES == null) throw new DataException("Impossible de charger la liste de voitures");
                if(RESERVATIONS == null) throw new DataException("Impossible de charger la liste de reservtions");
        }catch (DataException e){ 
            e.errorBox();
            e.close();
        }
        /* NOTE:  si les listes ne sont pas valide on quitte proprement, ici cela n a pas beaucoup de sens mais
                  imaginons que nos data se trouvent dans un fichier et que celui la est corrompu */
    }
    public ArrayList<Membre> getListeClient(){ return CLIENTS;}
    public ArrayList<Reservation> getListeReservation(){ return RESERVATIONS;}
    public ArrayList<Voiture> getListeVoiture(){
        if (VOITURES.isEmpty()) VOITURES.add(new Voiture("Plus de voiture disponible", 0));
        return VOITURES;
    } 
    public void ajouterClient(Membre inscriptionClient){CLIENTS.add(inscriptionClient);}
    public void ajouterReservation(Reservation reservation){
        VOITURES.remove(reservation.getVoiture());
        RESERVATIONS.add(reservation);
    }
    public boolean effacerReservation(Reservation reservation){
        if(this.getListeReservation().isEmpty()) return false;
        this.getListeReservation().remove(reservation);
        return true;
    }
    public boolean plusDeVoiture(){return this.getListeVoiture().get(0).getPlace()==0;}
}
