/**
 *  RESERVATION
 *  Stock les donnée d' un reservation de voiture
 *  @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo3.backend;

public class Reservation {
        private final Membre refClient; 
        private final Voiture refVoiture;
        
        public Reservation(Membre refClient,Voiture refVoiture){
            this.refClient= refClient;
            this.refVoiture=refVoiture;
        }
        public Voiture getVoiture(){return this.refVoiture;}
        public String getClient(){return this.refClient.getNom();}
        public float getPrix(){return this.refClient.calculerLocationPour(this.refVoiture);}
}
