/**
 *  PREMIUM
 * 
 * Perùet de generer un membre de type premium
 * NOTE: Membre est ici indispansable
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo3.backend;

public class Premium extends Membre implements Prix {
    public Premium(String nom){super(nom); /* Appel au constructeur Membre */}
    @Override
    public float calculerLocationPour(Voiture v) { return BASE + (v.getPlace() * PRIX_PREMIUM); }
    @Override
    public String getType(){ return "PREMIUM"; }
}
