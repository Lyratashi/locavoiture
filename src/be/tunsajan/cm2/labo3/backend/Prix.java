/**
 * Interface prix
 * contient la liste des prix 
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo3.backend;

public interface Prix {
    final int BASE= 50;
    final int PRIX_GOLD=5;
    final int PRIX_NORMAL=5;
    final int PRIX_PREMIUM = -2;
}
