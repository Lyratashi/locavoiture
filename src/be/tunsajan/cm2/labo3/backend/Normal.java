/**
 *  NORMAL
 * 
 * Perùet de generer un membre de type normal
 * NOTE: Membre est ici indispansable
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo3.backend;

public class Normal extends Membre implements Prix {
    public Normal(String nom) { super( nom); /* Appel au constructeur Membre */}
    @Override
    public float calculerLocationPour(Voiture v) { return BASE + (v.getPlace() * PRIX_NORMAL); }
    @Override 
    public String getType(){ return "NORMAL";}
}
