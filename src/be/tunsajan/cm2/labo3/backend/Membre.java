/**
 * Membre
 * permet de cree un membre
 * 
 * Attention: cette classe est abstraite et donc ne peut pas être instancier
 *            pour cela on est obliger de passer par ces enfants, PREMIUM, GOLD, NORMAL
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo3.backend;

public abstract class Membre {
    private final String NOM;
      
    public Membre(String nom){this.NOM=nom;}
    public final String getNom(){return this.NOM;}
    
    /* NOTE: Methode abstraite ce qui oblige les classes hérité a implementer ces methodes qui seronr
             surchargées
    */
    public abstract float calculerLocationPour(Voiture v);
    public abstract String getType(); 
        
    
}
