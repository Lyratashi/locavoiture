/**
 *  GOLD
 * 
 * Perùet de generer un membre de type gold
 * NOTE: Membre est ici indispansable
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo3.backend;

public class Gold extends Membre implements Prix{
    
    public Gold(String nom){super( nom); /* Appel au constructeur Membre */}
    @Override
    public float calculerLocationPour(Voiture v){return (v.getPlace()*PRIX_GOLD); }
    @Override
    public String getType(){ return "GOLD";}
    
} 
