/**
 * DataException 
 * Gestionnaire d exception pour les datas
 * @author Somboom TUNSAJAN
 */
package be.tunsajan.cm2.labo3.backend;

import helmo.nhpack.NHPack;

/**
 *
 * @author Lz
 */
public class DataException extends Exception{
    final private String MESSAGE;
    
    public DataException(String message){this.MESSAGE=message;}
    final public void errorBox(){NHPack.getInstance().showError("ERREUR", this.MESSAGE);}
    public void close(){System.exit(0);}
}
