/**
 *  interface Data
 * 
 * contient les constante permettant de stocker les données
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo3.backend;

import java.util.ArrayList;
import java.util.Arrays;

public interface Data {
  
    final Voiture TAB_VOITURE[]={new Voiture("Smart 4 Two", 2), 
                           new Voiture("Smart 4 Four", 4), 
                           new Voiture("Renault Megane",5),
                           new Voiture("Renault Espace",6)};
    
    final Membre TAB_MEMBRES[]={new Gold("TUNSAJAN"), 
                                 new Premium("BARBION"), 
                                 new Normal("WANG")};
    
    final public ArrayList<Voiture> VOITURES=new ArrayList(Arrays.asList(TAB_VOITURE));
    final public ArrayList<Membre> CLIENTS = new ArrayList(Arrays.asList(TAB_MEMBRES));
    final public ArrayList<Reservation> RESERVATIONS = new ArrayList();
}
