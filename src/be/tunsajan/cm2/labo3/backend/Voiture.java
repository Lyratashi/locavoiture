/**
 * Voiture
 * Permet de gerer une voiture
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo3.backend;

public class Voiture {
    final private int NB_PLACE;
    final private String MODELE;
    
    public Voiture(String m, int nbPlace){
        this.MODELE = m;
        this.NB_PLACE = nbPlace;
    }
    public int getPlace(){ return this.NB_PLACE;}
    public String getModele(){ return this.MODELE;}
    @Override
    public String toString(){
        if(this.getPlace() == 0) return "Plus de voiture disponible";
        return ""+this.getModele()+" "+this.getPlace()+" p";}
    
}
