/**
 * interface gestionaire
 * @author Somboom TUNSAJAN
 */
package be.tunsajan.cm2.labo3.controllers;

import be.tunsajan.cm2.labo3.backend.Gestionaire;

public interface GestionaireLocation {
    final public Gestionaire GESTIONAIRE=new Gestionaire();
    
}
