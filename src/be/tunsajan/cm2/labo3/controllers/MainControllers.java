/**
 *  MainController
 *  Permet de faire le lien entre le graphics et le programme 
 *
 */
package be.tunsajan.cm2.labo3.controllers;
import be.tunsajan.cm2.labo3.backend.Gold;
import be.tunsajan.cm2.labo3.backend.Reservation;
import be.tunsajan.cm2.labo3.backend.Membre;
import be.tunsajan.cm2.labo3.backend.Normal;
import be.tunsajan.cm2.labo3.backend.Premium;
import be.tunsajan.cm2.labo3.backend.Voiture;
import helmo.nhpack.NHPack;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 *
 * /!\ parfois il arrive qu une ewception se lance par rapport a la comboxbox mais cela fonctionne 
 *     parfaitement
 * /!\ J' ai encoder des membres en dur afin de facilité les tests mais l ajout de mmebre fonctionne parfaitement
 * NOTE: Merci a Benjamin BARBION de m avoir aider pour l interface graphique, mainteant je gere :p
 */
public class MainControllers implements GestionaireLocation{
    private String nom;
    private ArrayList<Membre> svMembre; /* selectedValues retourne une ArrayList */
    private Voiture svVoiture;
    
    public MainControllers(){this.svVoiture=new Voiture("null", 0);}
    public ArrayList<Membre> getListeClient(){return GESTIONAIRE.getListeClient();}
    public ArrayList<Reservation> getListeReservation(){return GESTIONAIRE.getListeReservation();}
    public ArrayList<Voiture> getListeVoiture(){ return GESTIONAIRE.getListeVoiture();}
    public String getTextFieldNom(){return this.nom;}
    public void setTextFieldNom(String nom){this.nom=nom;}
    public ArrayList<Membre> getSelectedValueClientList(){return this.svMembre;}
    public Voiture getSelectedValueVoiture(){return this.svVoiture;}
    public void setSelectedValueClientList(ArrayList<Membre> sm){this.svMembre=new ArrayList(sm);}
    public void setSelectedValueVoiture(Voiture sv){ this.svVoiture=sv;} 
    private static boolean stringChecker(String strChaine){ return (!Pattern.compile("[a-zA-Z ]*").matcher(strChaine).matches() || strChaine.charAt(0) == ' ') ;}
    
    public void textBoxAjouterReservation(){
        if(this.svMembre.isEmpty()){ 
            NHPack.getInstance().showError("ERREUR", "Pas de client selectioné");
            return;
        }
        if(GESTIONAIRE.plusDeVoiture()){
            NHPack.getInstance().showError("ERREUR", "Plus de voitures ");
            return;
        }
        GESTIONAIRE.ajouterReservation(new Reservation( this.svMembre.get(0), this.svVoiture));
        NHPack.getInstance().showInformation("Success", "Réservation prise en compte");
        NHPack.getInstance().refreshMainWindow();
    }
    
    public void textBoxAjouterClient(){
            NHPack.getInstance().loadWindow("be.tunsajan.cm2.labo3.frontend.ajouterClient.xml", this); 
            NHPack.getInstance().showWindow("be.tunsajan.cm2.labo3.frontend.ajouterClient.xml");
    }
    
    public void ajouterClient(int type){
        if(this.nom.isEmpty()||stringChecker(this.nom)){ /* Si pas de nom entré */
            NHPack.getInstance().showError("ERREUR", "Nom invalide");
            return;
        }
        switch(type){
            case 1: GESTIONAIRE.ajouterClient(new Normal(this.getTextFieldNom())); break;
            case 2: GESTIONAIRE.ajouterClient(new Premium(this.getTextFieldNom())); break;
            case 3: GESTIONAIRE.ajouterClient(new Gold(this.getTextFieldNom())) ; break;
        }
        this.nom = null; 
        NHPack.getInstance().closeWindow("be.tunsajan.cm2.labo3.frontend.ajouterClient.xml");
        NHPack.getInstance().showInformation("Success", "Inscription réussie ");
        NHPack.getInstance().refreshMainWindow();
    }
}

